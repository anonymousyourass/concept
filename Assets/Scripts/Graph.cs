﻿using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class Graph : MonoBehaviour
{


    public Text[] texts;
  
    private static Material lineMaterialC;
    private static Material lineMaterialR;


    private string[] dataLines;
    private string[] names;
    //item,time1,value1,quantity1,time2,value2,quantity2,time3,value3,quantity3,time4,value4,quantity4,time5,value5,quantity5,time6,value6,quantity6,volume;
    private TVQ[][] tvqData;
    private Rect rectangle;


    private class TVQ
    {
        public int time;
        public int value;
        public int quantity;
    }

    void Awake()
    {
        Debug.Log("init started " + Time.realtimeSinceStartup);

        //fill the particle helper vars
        var shader = Shader.Find("Hidden/Internal-Colored");

        lineMaterialC = new Material(shader);
        lineMaterialC.hideFlags = HideFlags.HideAndDontSave;
        lineMaterialC.color = Color.cyan;

        lineMaterialR = new Material(shader);
        lineMaterialR.hideFlags = HideFlags.HideAndDontSave;
        lineMaterialR.color = Color.red;


        var dataTXT = Resources.Load("csv") as TextAsset;
        dataLines = dataTXT.text.Split(';').Skip(1).ToArray();
        tvqData = new TVQ[dataLines.Length][];
        names = new string[dataLines.Length];

        System.Func<string[],bool> checkA = (a) =>
        {
            var ou = 0;
            for (int i = 1 ;i < a.Length - 1; i++ )
            {
                if (!int.TryParse(a[i], out ou))
                {
                   // Debug.Log("cant parse to int " + a[i]);
                    return false;
                }
            }
            return true;
        };
        
        for (int i = 0; i < dataLines.Length; i++)
        {
            var arrS = dataLines[i].Split(',');
            if (arrS.Length != 20)
            {
                arrS = dataLines[i-1].Split(',');
                continue;
            }
            names[i] = arrS[0];

            if (!checkA(arrS))
            {
                continue;
            }

            tvqData[i] = new TVQ[]
            {
                
                new TVQ(){time = int.Parse(arrS[1]), value = int.Parse(arrS[2]), quantity = int.Parse(arrS[3])}, 
                new TVQ(){time = int.Parse(arrS[4]), value = int.Parse(arrS[5]), quantity = int.Parse(arrS[6])},  
                new TVQ(){time = int.Parse(arrS[7]), value = int.Parse(arrS[8]), quantity = int.Parse(arrS[9])},  
                new TVQ(){time = int.Parse(arrS[10]), value = int.Parse(arrS[11]), quantity = int.Parse(arrS[12])}, 
                new TVQ(){time = int.Parse(arrS[13]), value = int.Parse(arrS[14]), quantity = int.Parse(arrS[15])},  
                new TVQ(){time = int.Parse(arrS[16]), value = int.Parse(arrS[17]), quantity = int.Parse(arrS[18])}, 
            };

        }


        maxV = tvqData.Where(t=>t!=null).Max(t => t[selectedTime].value);
        maxQ = tvqData.Where(t=>t!=null).Max(t => t[selectedTime].quantity);

        
    }

    const float speedX = 1f;
    const float speedY = 1f;
    const float speedZ = 20f;
    public Camera cam;
    
    private float maxV = 0f;
    private float maxQ = 0f;


    private int selectedTime = 0;

    float rectxMin = 0.33f;
    float rectyMin = 0.33f;

    private Vector3 LD, LU, RU, RD, L, U, R, D, LUD, LDD, RUD, RDD, camp;

    void Update()
    {

        LD = cam.ScreenToWorldPoint(new Vector3(rectangle.xMin, rectangle.yMin, cam.nearClipPlane + 0.01f));
        LU = cam.ScreenToWorldPoint(new Vector3(rectangle.xMin, rectangle.yMax, cam.nearClipPlane + 0.01f));

        RU = cam.ScreenToWorldPoint(new Vector3(rectangle.xMin + rectangle.width, rectangle.yMax, cam.nearClipPlane + 0.01f));
        RD = cam.ScreenToWorldPoint(new Vector3(rectangle.xMin + rectangle.width, rectangle.yMin, cam.nearClipPlane + 0.01f));

        L = cam.ScreenToWorldPoint(new Vector3(0f, 0f, -cam.transform.position.z));
        U = cam.ScreenToWorldPoint(new Vector3(0f, cam.pixelHeight, -cam.transform.position.z));

        R = cam.ScreenToWorldPoint(new Vector3(cam.pixelWidth, 0f, -cam.transform.position.z));
        D = cam.ScreenToWorldPoint(new Vector3(0f, 0f, -cam.transform.position.z));


        LUD = cam.ScreenToWorldPoint(new Vector3(rectangle.xMin, rectangle.yMax, -cam.transform.position.z));
        LDD = cam.ScreenToWorldPoint(new Vector3(rectangle.xMin, rectangle.yMin, -cam.transform.position.z));

        RUD = cam.ScreenToWorldPoint(new Vector3(rectangle.xMin + rectangle.width, rectangle.yMax, -cam.transform.position.z));
        RDD = cam.ScreenToWorldPoint(new Vector3(rectangle.xMin + rectangle.width, rectangle.yMin, -cam.transform.position.z));

        camp = cam.transform.position;

        if (Input.GetKey(KeyCode.Space))
        {
            texts[0].text = string.Empty;
            texts[1].text = string.Empty;
            texts[2].text = string.Empty;
            texts[3].text = string.Empty;

            texts[4].text = string.Empty;

            texts[5].text = string.Empty;
            texts[6].text = string.Empty;
            texts[7].text = string.Empty;
            texts[8].text = string.Empty;
        }
        else
        {
            texts[0].text = ("max " + maxQ.ToString() + "cur" + U.y);
            texts[1].text = (0.ToString() + "cur " + L.x);
            texts[2].text = ("max " + maxV.ToString() + "cur " + R.x);
            texts[3].text = (0.ToString() + "cur " + D.y);

            texts[4].text = camp.x.ToString("0.00") + ":" + camp.y.ToString("0.00");

            texts[5].text = "areamaxQ" + LUD.y;
            texts[6].text = "areaminV" + LDD.x;
            texts[7].text = "areamaxV" + RUD.x;
            texts[8].text = "areaminQ" + RDD.y;
        }
       

        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            rectxMin += 0.05f;
            rectyMin += 0.05f;
        }
        if (Input.GetKey(KeyCode.KeypadPlus))
        {
            rectxMin -= 0.05f;
            rectyMin -= 0.05f;
        }

        float rectyWidth = (cam.pixelWidth -  2*rectxMin * cam.pixelWidth);
        float rectyHeight = (cam.pixelHeight - 2* rectyMin * cam.pixelHeight);

        rectangle = new Rect(cam.pixelWidth * rectxMin, cam.pixelHeight * rectyMin, rectyWidth, rectyHeight);

        if (Input.GetMouseButton(0))
        {
            var dx = Input.GetAxis("Mouse X") * speedX;
            var dy = Input.GetAxis("Mouse Y") * speedY;
            transform.Translate(new Vector3(dx,dy, 0f));
        }

        var s = Input.GetAxis("Mouse ScrollWheel") * speedZ;
        transform.Translate(new Vector3(0f, 0f, s));

        if (Input.GetKey(KeyCode.Keypad0))
        {
            selectedTime = 0;
        }
        else if (Input.GetKey(KeyCode.Keypad1))
        {
            selectedTime = 1;
        }
        else if (Input.GetKey(KeyCode.Keypad2))
        {
            selectedTime = 2;
        }
        else if (Input.GetKey(KeyCode.Keypad3))
        {
            selectedTime = 3;
        }
        else if (Input.GetKey(KeyCode.Keypad4))
        {
            selectedTime = 4;
        }
        else if (Input.GetKey(KeyCode.Keypad5))
        {
            selectedTime = 5;
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
              transform.Translate(new Vector3(-0.01f,0f, 0f));
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(new Vector3(0.01f, 0f, 0f));
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(new Vector3(0f, 0.01f, 0f));
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(new Vector3(0f, -0.01f, 0f));
        }


    }

    const float e = 2.71828182845904523536028747135266249775724709369995f;


    private static Color col1 = new Color(0f, 0.5f, 0.5f);
    private static  Color col2 = new Color(0.13f, 0.5f, 0.5f);  
    private static  Color col3 = new Color(0.26f, 0.5f, 0.5f);
    private static  Color col4 = new Color(0.53f, 0.5f, 0.5f);
    private static  Color col5 = new Color(0.76f, 0.5f, 0.5f);
    private static  Color col6 = new Color(1f, 0.5f, 0.5f);

    private static Color[] colorDots = new Color[6] { col1, col2, col3, col4, col5, col6 };

    void OnPostRender()
    {
        

       


        drawRect(LD,LU,RD,RU,Color.white);

        drawAxis();

        for (int i = 0; i < tvqData.Length; i++)
        {
            if (tvqData[i] != null)
            {
                Vector3 p = new Vector3((float)tvqData[i][selectedTime].value, (float)tvqData[i][selectedTime].quantity, 0f);
                if (p.x < (RDD.x) && p.y < RUD.y)
                {
                    drawCircle(p, 0.01f,Color.cyan , 10,lineMaterialC);
                }
                else
                {
                    if (p.x >= RUD.x)
                    {
                        p = new Vector3(RDD.x + (p.x - RDD.x) / (maxV) * 25, p.y, p.z);
                    }
                    if (p.y >= RUD.y)
                    {
                        p = new Vector3(p.x, RUD.y + (p.y - RDD.y) / (maxQ) * 25, p.z);
                    }
                    drawCircle(p, 0.01f, Color.cyan, 10, lineMaterialR);
                }
               
            }
        }
    }
    bool b = false;
    void drawAxis()
    {
        GL.Begin(GL.LINES);

        GL.Color(Color.white);

        GL.Vertex3(transform.position.x - 1000f, transform.position.y, transform.position.z + 5f);
        GL.Vertex3(transform.position.x + 1000f, transform.position.y, transform.position.z + 5f);

        GL.Vertex3(transform.position.x, transform.position.y - 1000f, transform.position.z + 5f);
        GL.Vertex3(transform.position.x, transform.position.y + 1000f, transform.position.z + 5f);

        GL.End();
    }



    void drawRect(Vector3 LD,Vector3 LU,Vector3 RD,Vector3 RU,Color c)
    {


        GL.Color(c);
      
        GL.Begin(GL.LINES);
        
        GL.Vertex3(LD.x, LD.y, LD.z);
        GL.Vertex3(LU.x, LU.y, LU.z);

        GL.Vertex3(LU.x, LU.y, LU.z);
        GL.Vertex3(RU.x, RU.y, RU.z);

        GL.Vertex3(RU.x, RU.y, RU.z);
        GL.Vertex3(RD.x, RD.y, RD.z);

        GL.Vertex3(RD.x, RD.y, RD.z);
        GL.Vertex3(LD.x, LD.y, LD.z);

        GL.End();
    }

    public void drawCircle(Vector3 p,float radius,Color c,int lineC,Material mat)
    {
        if (float.IsInfinity(p.x) || float.IsInfinity(p.y) || float.IsInfinity(p.z))
        {
            return;
        }
        mat.SetPass(0);
        
        //GL.Color(mat.color); 
        GL.Begin(GL.LINES);
        for (int i = 0; i < lineC; ++i)
        {
            float a = i / (float)lineC;
            float prevAngle = (i - 1) / (float)lineC * (Mathf.PI * 2f);
            float angle = a * Mathf.PI * 2;
            GL.Vertex3(p.x + Mathf.Cos(prevAngle) * radius, p.y + Mathf.Sin(prevAngle) * radius, p.z + 0);
            GL.Vertex3(p.x + Mathf.Cos(angle) * radius, p.y + Mathf.Sin(angle) * radius, p.z);
        }

        GL.End();
      
    }
	
}
